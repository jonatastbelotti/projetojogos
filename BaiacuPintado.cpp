#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "BaiacuPintado.hpp"

BaiacuPintado::BaiacuPintado(SDL_Renderer* tela, int l, int a, int f):Peixe(tela, l, a, f){
  switch(fase){
    case 1:
      largura = (int)252/1.5;
      altura = (int)107/1.5;
      y = alturaTela + 100;
      x = (largura/2) + (rand()%(larguraTela-largura));
      velX = 40.0;
      tempoImagem = 0.4;
      break;
    case 2:
      break;
  }

  carregarImagem();
}

void BaiacuPintado::carregarImagem(){
  SDL_Surface* img;
  bool r = true;

  char caminhoD[] = "Imagens/baiacu0_D.png";
  char caminhoE[] = "Imagens/baiacu0_E.png";
  for(int i = 0; i < 4; i++){
    caminhoD[14] = 48 + i;
    caminhoE[14] = 48 + i;
    img = IMG_Load(caminhoD);
    vetImagensD[i] = SDL_CreateTextureFromSurface(gRenderer, img);
    SDL_FreeSurface(img);
    img = IMG_Load(caminhoE);
    vetImagensE[i] = SDL_CreateTextureFromSurface(gRenderer, img);
    SDL_FreeSurface(img);

    r = (r && vetImagensD[i] != NULL && vetImagensD[i] != NULL);
  }

  if(!r){
    printf("Erro ao carregar imagens do Baiacu!\n");
  }
}

void BaiacuPintado::mover(unsigned int temp, int xMergulhador, int yMergulhador){
  if(yMergulhador >= y){
    if(direcao == 1){
      moverDireita(temp);
    }else{
      moverEsquerda(temp);
    }
  }else{
    if(xMergulhador > x){
      moverDireita(temp);
      direcao = 1;
    }else{
      moverEsquerda(temp);
      direcao = 2;
    }
  }
}

void BaiacuPintado::run(unsigned int temp, int xMergulhador, int yMergulhador){
  if(fase == 1){
    switch(estado){
      case ANDANDO:
        mover(temp, xMergulhador, yMergulhador);
        break;
      case PARADO:
        break;
    }
  }else{

  }

}
