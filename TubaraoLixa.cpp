#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "TubaraoLixa.hpp"

TubaraoLixa::TubaraoLixa(SDL_Renderer* tela, int l, int a, int f):Peixe(tela, l, a, f){
  if(fase == 1){
    largura = 430/3;
    altura = 250/3;
    y = alturaTela + 100;
    x = (largura/2) + (rand()%(larguraTela-largura));
    velX = 60.0;
    tempoImagem = 0.3;

    carregarImagem();
  }
}

void TubaraoLixa::carregarImagem(){
  SDL_Surface* img;
  bool r = true;

  char caminhoD[] = "Imagens/tubaraoLixa0_D.png";
  char caminhoE[] = "Imagens/tubaraoLixa0_E.png";
  for(int i = 0; i < 4; i++){
    caminhoD[19] = 48 + i;
    caminhoE[19] = 48 + i;
    img = IMG_Load(caminhoD);
    vetImagensD[i] = SDL_CreateTextureFromSurface(gRenderer, img);
    SDL_FreeSurface(img);
    img = IMG_Load(caminhoE);
    vetImagensE[i] = SDL_CreateTextureFromSurface(gRenderer, img);
    SDL_FreeSurface(img);

    r = (r && vetImagensD[i] != NULL && vetImagensE[i] != NULL);
  }

  if(!r){
    printf("Erro ao carregar imagens do Tubarao Lixa!\n");
  }
}

void TubaraoLixa::run(unsigned int temp, int xMergulhador, int yMergulhador){
  switch(estado){
    case ANDANDO:
      mover(temp);
      break;
    case PARADO:
      break;
  }
}
