#include <stdio.h>
#include "Mergulhador.hpp"

Mergulhador::Mergulhador(SDL_Renderer* te, int lar, int alt, int f):Objeto(te, lar, alt){
  fase = f;

  if(fase == 1){
    estado = CAINDO;
    altura = (int)(565/3);
    largura = (int)(313/3);
    x = (larguraTela/2);
    y = (alturaTela/2)-100;
    moveuUltimaVez = desenhadoUltimaVez = SDL_GetTicks();
    velX = 150.0;
    velY = 50.0;
    imagem = 0;
    oxigenio = 5;
    vetImagens[0] = vetImagens[1] = vetImagens[2] = vetImagens[3] = NULL;
  }

  carregarImagem();
}

void Mergulhador::carregarImagem(){
  bool r = true;
  SDL_Surface* img;
  char caminho[] = "Imagens/mergulhadorBF0_0.png";
  caminho[21] = 48+fase;

  for(int i = 0; i < 4; i++){
    caminho[23] = 48+i;
    img = IMG_Load(caminho);
    vetImagens[i] = SDL_CreateTextureFromSurface(gRenderer, img);
    SDL_FreeSurface(img);
    r = (vetImagens[i] != NULL && r);
  }

  if(!r){
    printf("Erro ao carregar as imagens do mergulhador!\n");
  }

}

void Mergulhador::desenha(){
  unsigned int temp = SDL_GetTicks();
  float t = (float)(temp-desenhadoUltimaVez)/1000;
  if(t > 0.4){
    imagem++;
    desenhadoUltimaVez = temp;
  }
  SDL_Rect rect = {(int)x-(largura/2), (int)y-(altura/2), largura, altura};
  SDL_RenderCopy(gRenderer, vetImagens[(imagem%4)], NULL, &rect);
}

void Mergulhador::run(unsigned int temp){
  switch(estado){
    case DIREITA:
      moverDireita(temp);
      moveuUltimaVez = temp;
      break;
    case ESQUERDA:
      moverEsquerda(temp);
      moveuUltimaVez = temp;
    case CIMA:
      moverCima(temp);
      moveuUltimaVez = temp;
    case BAIXO:
      moverBaixo(temp);
      moveuUltimaVez = temp;
      break;
    case CIMAESQUERDA:
      moverCima(temp);
      moverEsquerda(temp);
      moveuUltimaVez = temp;
    case BAIXOESQUERDA:
      moverBaixo(temp);
      moverEsquerda(temp);
      moveuUltimaVez = temp;
      break;
    case CIMADIREITA:
      moverCima(temp);
      moverDireita(temp);
      moveuUltimaVez = temp;
    case BAIXODIREITA:
      moverBaixo(temp);
      moverDireita(temp);
      moveuUltimaVez = temp;
      break;
  }
  moveuUltimaVez = temp;
}

void Mergulhador::bonusOxigenio(int quant){
}

void Mergulhador::mover(int direcao){
  switch(direcao){
    case 1:
      if(estado == CIMA){estado = CIMADIREITA;}
      if(estado == BAIXO){estado = BAIXODIREITA;}
      if(estado == CAINDO){estado = DIREITA;}
      break;
    case 2:
      if(estado == CIMA){estado = CIMAESQUERDA;}
      if(estado == BAIXO){estado = BAIXOESQUERDA;}
      if(estado == CAINDO){estado = ESQUERDA;}
      break;
    case 3:
      if(estado == ESQUERDA){estado = CIMAESQUERDA;}
      if(estado == DIREITA){estado = CIMADIREITA;}
      if(estado == CAINDO){estado = CIMA;}
      break;
    case 4:
      if(estado == ESQUERDA){estado = BAIXOESQUERDA;}
      if(estado == DIREITA){estado = BAIXODIREITA;}
      if(estado == CAINDO){estado = BAIXO;}
      break;
    case -1:
      if(estado == CIMADIREITA){estado = CIMA;}
      if(estado == BAIXODIREITA){estado = BAIXO;}
      if(estado == DIREITA){estado = CAINDO;}
      break;
    case -2:
      if(estado == CIMAESQUERDA){estado = CIMA;}
      if(estado == BAIXOESQUERDA){estado = BAIXO;}
      if(estado == ESQUERDA){estado = CAINDO;}
      break;
    case -3:
      if(estado == CIMADIREITA){estado = DIREITA;}
      if(estado == CIMAESQUERDA){estado = ESQUERDA;}
      if(estado == CIMA){estado = CAINDO;}
      break;
    case -4:
      if(estado == BAIXODIREITA){estado = DIREITA;}
      if(estado == BAIXOESQUERDA){estado = ESQUERDA;}
      if(estado == BAIXO){estado = CAINDO;}
      break;
  }
}

void Mergulhador::moverBaixo(unsigned int temp){
  unsigned int t = (temp - moveuUltimaVez);

  float dx = (float)((float)t/1000) * velY;
  if(y+dx < (alturaTela-(altura/2))){
    y += dx;
  }
}

void Mergulhador::moverCima(unsigned int temp){
  unsigned int t = (temp - moveuUltimaVez);

  float dx = (float)((float)t/1000) * velY;
  if(y-dx > (0 + (altura/2))){
    y -= dx;
  }
}

void Mergulhador::moverDireita(unsigned int temp){
  unsigned int t = (temp - moveuUltimaVez);

  float dy = (float)((float)t/1000) * velX;
  if(x+dy < (larguraTela-(largura/2))){
    x += dy;
  }
}

void Mergulhador::moverEsquerda(unsigned int temp){
  unsigned int t = (temp - moveuUltimaVez);

  float dy = (float)((float)t/1000) * velX;
  if(x-dy > (0+(largura/2))){
    x -= dy;
  }
}
