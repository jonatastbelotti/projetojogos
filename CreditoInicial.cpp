#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>
#include <time.h>

#include "CreditoInicial.hpp"

CreditoInicial::CreditoInicial(int larg, int  alt, SDL_Renderer* tela){
  gRenderer = tela;
  alturaTela = alt;
  larguraTela = larg;
  fundo = NULL;
  carregarImagem();
}

void CreditoInicial::carregarImagem(){
  fundo = SDL_CreateTextureFromSurface(gRenderer,  IMG_Load("Imagens/creditosiniciais.png"));
  if(fundo == NULL){
    printf("Erro ao carregar imagem da tela Credito Inicial!\n");
  }
}

int CreditoInicial::run(){
  int r = 1;
  SDL_Event e;
  unsigned int temp, ini;

  ini = temp = SDL_GetTicks();



  while(r>0 && (float)((temp-ini)/1000) < 5){
    while(SDL_PollEvent(&e) != 0){
      if(e.type == SDL_QUIT){
        r = -1;;
      }
    }
    SDL_RenderClear(gRenderer);
    SDL_RenderCopy(gRenderer, fundo, NULL, NULL);
    SDL_RenderPresent(gRenderer);
    temp = SDL_GetTicks();
  }
  SDL_RenderClear(gRenderer);
  SDL_RenderPresent(gRenderer);

  return r;
}
