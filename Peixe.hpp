#ifndef PEIXE_H
#define PEIXE_H

#include <SDL2/SDL_image.h>
#include "Objeto.hpp"

#define PARADO 0
#define ANDANDO 1


class Peixe : public Objeto{
public:
	int direcao;
	int fase;
	int imagem;
	int estado;
	float tempoImagem;
	SDL_Texture* vetImagensD[4];
	SDL_Texture* vetImagensE[4];


public:
	Peixe(SDL_Renderer* tela, int larg, int alt, int f);
	virtual void carregarImagem() = 0;
	void desenha();
	void mover(unsigned int);
	void moverDireita(unsigned int);
	void moverEsquerda(unsigned int);
	virtual void run(unsigned int, int, int) = 0;
};
#endif
