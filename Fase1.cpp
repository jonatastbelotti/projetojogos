#include "Fase1.hpp"

Fase1::Fase1(int lar, int alt, SDL_Renderer* tela){
  srand((unsigned)time(NULL));
  alturaTela = alt;
  larguraTela = lar;
  gRenderer = tela;
  fundoTela = imagemOxigenio = NULL;
  rect = {0, 0, 1000, 600};
  velFundoY = 20.0;
  y1 = 0;
  y2 = alturaTela;
  jaAndou = 0.0;
  ultMovFundo = SDL_GetTicks();
  ultLixCria = ultPexCria = -2000;
  mergulhador = new Mergulhador(tela, lar, alt, 1);
  venceu = 0;

  carregarImagem();
}

void Fase1::carregarImagem(){
  SDL_Surface* img;
  img = IMG_Load("Imagens/fundomar.jpg");
  fundoTela = SDL_CreateTextureFromSurface(gRenderer, img);
  SDL_FreeSurface(img);

  img = IMG_Load("Imagens/oxigenio.png");
  imagemOxigenio = SDL_CreateTextureFromSurface(gRenderer, img);
  SDL_FreeSurface(img);

  if(fundoTela == NULL || imagemOxigenio == NULL){
    printf("Erro ao carregar imagens da Fase 1!\n");
  }
}

void Fase1::desenha(){
  desenhaFundo();
  mergulhador->desenha();
  desenhaLixos();
  desenhaPeixes();
  desenhaPlacar();
}


void Fase1::desenhaPlacar(){
  rect = {(larguraTela-60), 10, 40, 70};

  for(int i = 0; i < mergulhador->oxigenio; i++){
    SDL_RenderCopy(gRenderer, imagemOxigenio, NULL, &rect);
    rect.x -= 40;
  }

}

void Fase1::desenhaFundo(){
  rect = {0, y1, 1000, 600};
  SDL_RenderCopy(gRenderer, fundoTela, NULL, &rect);
  rect = {0, y2, 1000, 600};
  SDL_RenderCopy(gRenderer, fundoTela, NULL, &rect);
}

void Fase1::desenhaLixos(){
  for(int i = 0; i < listaLixos.size(); i++){
    listaLixos.at(i)->desenha();
  }
}

void Fase1::desenhaPeixes(){
  for(int i = 0; i < listaPeixes.size(); i++){
    listaPeixes.at(i)->desenha();
  }
}

void Fase1::movimenta(){
  movimentaFundo();
  unsigned int t = SDL_GetTicks();
  mergulhador->run(t);
  runPeixes(t);
}

void Fase1::movimentaFundo(){
  unsigned int temp = SDL_GetTicks();

  unsigned int t = (temp - ultMovFundo);
  ultMovFundo = temp;

  float dy = (float)((float)t/1000) * velFundoY;

  y1 -= dy;
  y2 -= dy;
  jaAndou += dy;

  if(y1 <= (alturaTela*(-1))){y1 = alturaTela;}
  if(y2 <= (alturaTela*(-1))){y2 = alturaTela;}

  movimentaObjetos(dy);
}

void Fase1::movimentaObjetos(float dy){
  for(int i = 0; i < listaLixos.size(); i++){
    listaLixos.at(i)->y -= (3*dy);
  }
  for(int i = 0; i < listaPeixes.size(); i++){
    listaPeixes.at(i)->y -= 3*dy;
  }
}

void Fase1::colisao(){
  for(int i = 0; i < listaLixos.size(); i++){
    if(Objeto::colidiu(mergulhador, listaLixos.at(i))){
      morreu();
    }
  }

  for(int i = 0; i < listaPeixes.size(); i++){
    if(Objeto::colidiu(mergulhador, listaPeixes.at(i))){
      morreu();
    }
  }
}

void Fase1::runPeixes(unsigned int t){
  for(int i = 0; i < listaPeixes.size(); i++){
    listaPeixes.at(i)->run(t,mergulhador->x, mergulhador->y);
  }
}

void Fase1::destroi(){
  for(int i = 0; i < listaLixos.size(); i++){
    if(listaLixos.at(i)->y < (0 - (listaLixos.at(i)->altura) / 2)){
      listaLixos.erase(listaLixos.begin()+i);
      i--;
    }
  }
  for(int i = 0; i < listaPeixes.size(); i++){
    if(listaPeixes.at(i)->y < (0 - (listaPeixes.at(i)->altura) / 2)){
      listaPeixes.erase(listaPeixes.begin()+i);
      i--;
    }
  }
}

void Fase1::criaLixos(){
  unsigned int temp = SDL_GetTicks();

  unsigned int t = (temp - ultLixCria);
  if((float)(t/1000) >= 4.0){
    int num = 1 + rand()%6;
    for(int i = 0; i < num; i++){
      listaLixos.push_back(new Lixo(gRenderer, larguraTela, alturaTela));
    }
    ultLixCria = temp;
  }
}

void Fase1::criaObjetos(){
  if(jaAndou < (int)(FINALTELA/2)){
    criaLixos();
  }else{
    if(jaAndou < FINALTELA){
      criaPeixes();
    }
  }
}

void Fase1::criaPeixes(){
  unsigned int temp = SDL_GetTicks();

  unsigned int t = (temp - ultPexCria);
  if((float)(t/1000) >= 5.0){
    int t = rand()%5;

    if(t < 2){
      listaPeixes.push_back(new Pampo(gRenderer, larguraTela, alturaTela, 1));
    }
    if(t>1 && t<4){
      listaPeixes.push_back(new TubaraoLixa(gRenderer, larguraTela, alturaTela, 1));
    }
    if(t == 4){
      listaPeixes.push_back(new BaiacuPintado(gRenderer, larguraTela, alturaTela, 1));
    }
    ultPexCria = temp;
  }
}

void Fase1::morreu(){
  unsigned int t0, t1, d0, d1;
  t0 = t1 = d0 = d1 = SDL_GetTicks();
  int aux = 40;

  int n = (mergulhador->oxigenio)-1;
  mergulhador->oxigenio = n;

  while((t1-t0)/1000 < 5){
    t1 = d1 = SDL_GetTicks();

    if((d1-d0)/1000 > 0.2){
      (mergulhador->largura) += aux;
      (mergulhador->altura) += aux;
      aux *= -1;
      d0 = d1;
    }

    SDL_RenderClear(gRenderer);
    desenhaFundo();
    desenhaLixos();
    desenhaPeixes();
    mergulhador->desenha();
    desenhaPlacar();
    SDL_RenderPresent(gRenderer);
  }

  listaLixos.clear();
  listaPeixes.clear();
  jaAndou = 0;
  free(mergulhador);

  mergulhador = new Mergulhador(gRenderer, larguraTela, alturaTela, 1);
  mergulhador->oxigenio = n;
  y1 = 0;
  y2 = alturaTela;
  ultLixCria = SDL_GetTicks();
}

int Fase1::run(){
  int r = -2;

  SDL_Event e;
  while((r == -2) && (mergulhador->oxigenio)>0){
    while(SDL_PollEvent(&e) != 0){
      if(e.type == SDL_QUIT){
        r = -1;
      }else{
        if(e.type == SDL_KEYDOWN){
          if(e.key.keysym.sym == SDLK_RIGHT){
            mergulhador->mover(1);
          }
          if(e.key.keysym.sym == SDLK_LEFT){
            mergulhador->mover(2);
          }
          if(e.key.keysym.sym == SDLK_UP){
            mergulhador->mover(3);
          }
          if(e.key.keysym.sym == SDLK_DOWN){
            mergulhador->mover(4);
          }
        }
        if(e.type == SDL_KEYUP){
          if(e.key.keysym.sym == SDLK_RIGHT){
            mergulhador->mover(-1);
          }
          if(e.key.keysym.sym == SDLK_LEFT){
            mergulhador->mover(-2);
          }
          if(e.key.keysym.sym == SDLK_UP){
            mergulhador->mover(-3);
          }
          if(e.key.keysym.sym == SDLK_DOWN){
            mergulhador->mover(-4);
          }
        }
      }
    }

    if(jaAndou > FINALTELA+500){r = 1;}
    if(mergulhador->oxigenio == 0){r = 0;}

    SDL_RenderClear(gRenderer);
    desenha();
    SDL_RenderPresent(gRenderer);
    criaObjetos();
    movimenta();
    destroi();
    colisao();
  }

  return r;
}
