#ifndef OBJETO_H
#define OBJETO_H

#include <SDL2/SDL.h>

class Objeto{
public:
  SDL_Renderer* gRenderer;
  float x;
  float y;
  float velX;
  float velY;
  int larguraTela;
  int alturaTela;
  int altura;
  int largura;
  unsigned int desenhadoUltimaVez;
  unsigned int moveuUltimaVez;

public:
  Objeto();
  Objeto(SDL_Renderer*, int, int);
  static bool colidiu(Objeto*, Objeto*);
};
#endif
