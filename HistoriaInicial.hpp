#ifndef HISTORIA_INICIAL_H
#define HISTORIA_INICIAL_H

#include <SDL2/SDL.h>

class HistoriaInicial{
private:
  SDL_Renderer* gRenderer;
  int alturaTela;
  int larguraTela;

public:
  HistoriaInicial();
};
#endif
