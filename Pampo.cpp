#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Pampo.hpp"

Pampo::Pampo(SDL_Renderer* tela, int l, int a, int f):Peixe(tela, l, a, f){
  largura = 430/3;
  altura = 250/3;
  y = alturaTela + 100;
  x = (largura/2) + (rand()%(larguraTela-largura));
  velX = 250.0;
  tempoImagem = 0.2;

  carregarImagem();
}

void Pampo::carregarImagem(){
  SDL_Surface* img;
  bool r = true;

  char caminhoD[] = "Imagens/pampo0_D.png";
  char caminhoE[] = "Imagens/pampo0_E.png";
  for(int i = 0; i < 4; i++){
    caminhoD[13] = 48 + i;
    caminhoE[13] = 48 + i;
    img = IMG_Load(caminhoD);
    vetImagensD[i] = SDL_CreateTextureFromSurface(gRenderer, img);
    SDL_FreeSurface(img);
    img = IMG_Load(caminhoE);
    vetImagensE[i] = SDL_CreateTextureFromSurface(gRenderer, img);
    SDL_FreeSurface(img);

    r = (r && vetImagensD[i] != NULL && vetImagensE[i] != NULL);
  }

  if(!r){
    printf("Erro ao carregar imagens do Pampo!\n");
  }
}

void Pampo::run(unsigned int temp, int xMergulhador, int yMergulhador){
  switch(estado){
    case ANDANDO:
      mover(temp);
      break;
    case PARADO:
      break;
  }

}
