#ifndef LIXO_H
#define LIXO_H


#include "Objeto.hpp"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

class Lixo : public Objeto{
public:
	int tipo;
	SDL_Texture* imagem;

public:
	Lixo(SDL_Renderer*, int, int);
	void carregarImagem();
	void desenha();

};
#endif
