#ifndef PAMPO_H
#define PAMPO_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Peixe.hpp"

class Pampo : public Peixe{
public:
  Pampo(SDL_Renderer* tela, int l, int a, int f);
  void carregarImagem();
  void run(unsigned int, int, int);
};
#endif
