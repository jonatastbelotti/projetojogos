#include <stdio.h>
#include <SDL2/SDL.h>

#include "Fase1.hpp"
#include "Fase2.hpp"
#include "Fase3.hpp"
#include "Menu.hpp"
#include "CreditoInicial.hpp"
#include "CreditoFinal.hpp"
#include "HistoriaFinal.hpp"
#include "HistoriaInicial.hpp"
#include "Cardapio.hpp"
#include "Controles.hpp"

#define CREDITOINICIAL 0
#define MENU 1
#define HISTORIAINICIAL 2
#define FASE1 3
#define FASE2 4
#define FASE3 5
#define HISTORIAFINAL 6
#define CREDITOFINAL 7
#define CONTROLES 8
#define CARDAPIO 9

const int SCREEN_WIDTH = 1000;
const int SCREEN_HEIGHT = 600;

SDL_Window* gWindow = NULL;         //The window we'll be rendering to
SDL_Surface* gScreenSurface = NULL; //The surface contained by the window
SDL_Renderer* gRenderer = NULL;     //The window renderer



bool init(){
  //Initialization flag
  bool success = true;

  //Initialize SDL
  if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
    printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
    success = false;
  }
  else{
    //Create window
    gWindow = SDL_CreateWindow( "JOGO", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
    if( gWindow == NULL ){
      printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
      success = false;
    }
    else{
      //Create renderer for window
      gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
      if( gRenderer == NULL ){
        printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
        success = false;
      }
      else{
        //Initialize renderer color
        SDL_SetRenderDrawColor( gRenderer, 0x0, 0x0, 0x0, 0xFF );
      }
    }
  }

  return success;
}

void close(){
  //Destroy window
  SDL_DestroyWindow( gWindow );
  gWindow = NULL;

  //Quit SDL subsystems
  SDL_Quit();
}

int main(){
  int estado = CREDITOINICIAL;

  init();
  while(estado >= 0){
    switch(estado){
      case CREDITOINICIAL:{
        CreditoInicial creditoInicial(SCREEN_WIDTH, SCREEN_HEIGHT, gRenderer);
        if(creditoInicial.run() == -1){
          estado = -1;
        }else{
          estado = HISTORIAINICIAL;
        }
        break;
      }
      case MENU:{
        estado = FASE1;
        break;
      }
      case HISTORIAINICIAL:{
        estado = MENU;
        break;
      }
      case FASE1:{
        Fase1 fase1(SCREEN_WIDTH, SCREEN_HEIGHT, gRenderer);
        int r = fase1.run();
        if(r == 1){
          estado = FASE2;
        }
        if(r == -1){
          estado = -1;
        }
        if(r == 0){
          estado = MENU;
        }
        break;
      }
      case FASE2:{
        estado = 1;
        break;
      }
      case FASE3:{
        estado = 1;
        break;
      }
      case HISTORIAFINAL:{
        estado = 1;
        break;
      }
      case CREDITOFINAL:{
        CreditoFinal credFinal(SCREEN_WIDTH, SCREEN_HEIGHT, gRenderer);
        if(credFinal.run() == -1){
          estado = -1;
        }else{
          estado = 1;
        }
        break;
      }
      case CONTROLES:{
        estado = 1;
        break;
      }
      case CARDAPIO:{
        estado = 1;
        break;
      }
    }

  }
  close();
  return 0;
}
