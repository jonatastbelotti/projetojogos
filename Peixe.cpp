#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Peixe.hpp"

Peixe::Peixe(SDL_Renderer* tela, int larg, int alt, int f):Objeto(tela, larg, alt){
  fase = f;
  imagem = 0;
  vetImagensD[0] = vetImagensD[1] = vetImagensD[2] = vetImagensD[3] = NULL;
  vetImagensE[0] = vetImagensE[1] = vetImagensE[2] = vetImagensE[3] = NULL;
  estado = ANDANDO;
  direcao = rand()%2;
  moveuUltimaVez = SDL_GetTicks();
}

void Peixe::desenha(){
  unsigned int temp = SDL_GetTicks();
  float t = (float)(temp-desenhadoUltimaVez)/1000;

  if(t > tempoImagem){
    imagem++;
    desenhadoUltimaVez = temp;
  }

  SDL_Rect rect = {(int)x-(largura/2), (int)y-(altura/2), largura, altura};

  if(direcao == 1){
    SDL_RenderCopy(gRenderer, vetImagensD[imagem%4], NULL, &rect);
  }else{
    SDL_RenderCopy(gRenderer, vetImagensE[imagem%4], NULL, &rect);
  }
}

void Peixe::mover(unsigned int temp){
  if(direcao == 1){
    moverDireita(temp);
  }else{
    moverEsquerda(temp);
  }
}

void Peixe::moverDireita(unsigned int temp){
  unsigned int t = (temp - moveuUltimaVez);

  float dy = (float)((float)t/1000) * velX;
  if(x+dy < (larguraTela-(largura/2))){
    x += dy;
  }else{
    direcao = -1;
  }
  moveuUltimaVez = temp;
}

void Peixe::moverEsquerda(unsigned int temp){
  unsigned int t = (temp - moveuUltimaVez);

  float dy = (float)((float)t/1000) * velX;
  if(x-dy > (largura/2)){
    x -= dy;
  }else{
    direcao = 1;
  }
  moveuUltimaVez = temp;
}
