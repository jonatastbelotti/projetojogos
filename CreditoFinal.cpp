#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "CreditoFinal.hpp"

CreditoFinal::CreditoFinal(int larg, int alt, SDL_Renderer* tela){
  alturaTela = alt;
  larguraTela = larg;
  gRenderer = tela;
  imagem = NULL;
  y = alturaTela;
  velocidadeY = 40.0;
  creditos = {(larguraTela/2) - (774/2), alturaTela,774, 1500};
  ultimoMovimento = SDL_GetTicks();
  // clock_gettime(CLOCK_MONOTONIC_RAW, &ultimoMovimento);
  carregarImagem();
}

void CreditoFinal::carregarImagem(){
  imagem = SDL_CreateTextureFromSurface(gRenderer,  IMG_Load("Imagens/creditofinal.png"));

  if(imagem == NULL){
    printf("Erro ao carregar imagem da tela Credito Inicial!\n");
  }

}


int CreditoFinal::run(){
  int r = 1;
  SDL_Event e;


  SDL_RenderClear(gRenderer);
  SDL_RenderCopy(gRenderer, imagem, NULL, &creditos);
  SDL_RenderPresent(gRenderer);
  while(r>0){
    while(SDL_PollEvent(&e) != 0){
      if(e.type == SDL_QUIT){
        r = -1;
      }
    }

      SDL_RenderClear(gRenderer);
      SDL_RenderCopy(gRenderer, imagem, NULL, &creditos);
      SDL_RenderPresent(gRenderer);
      moveTextos();

    if(creditos.y < -1500){
      r = 0;
    }
  }

  return r;
}


void CreditoFinal::moveTextos(){
  unsigned int temp = SDL_GetTicks();

  int n = creditos.y;

  unsigned int t = (temp - ultimoMovimento);
  ultimoMovimento = temp;

  float dy = (float)((float)t/1000) * velocidadeY;
  y -= dy;

  creditos.y = (int)y;

}
