#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>

#include "Lixo.hpp"

Lixo::Lixo(SDL_Renderer* tela, int larg, int alt):Objeto(tela, larg, alt){
  tipo = rand()%7;
  imagem = NULL;

  carregarImagem();
}

void Lixo::carregarImagem(){
  SDL_Surface* img;

  switch(tipo){
    case 0:
      img = IMG_Load("Imagens/lixo0.png");
      largura = 205.0/2;
      altura = 84.0/2;
      break;
    case 1:
      img = IMG_Load("Imagens/lixo1.png");
      largura = 84.0/2;
      altura = 136.0/2;
      break;
    case 2:
      img = IMG_Load("Imagens/lixo2.png");
      largura = 113.0/2;
      altura = 152.0/2;
      break;
    case 3:
      img = IMG_Load("Imagens/lixo3.png");
      largura = 75.0/2;
      altura = 117.0/2;
      break;
    case 4:
      img = IMG_Load("Imagens/lixo4.png");
      largura = 134.0/2;
      altura = 190.0/2;
      break;
    case 5:
      img = IMG_Load("Imagens/lixo5.png");
      largura = 77.0/2;
      altura = 195.0/2;
      break;
    case 6:
      img = IMG_Load("Imagens/lixo6.png");
      largura = 169.0/2;
      altura = 133.0/2;
      break;
    default:
      printf("Erro, lixo nao definido!\n");
      break;

  }

  y = alturaTela + (altura/2) + rand()%100;
  x = (largura/2) + (rand()%(larguraTela-largura));

  imagem = SDL_CreateTextureFromSurface(gRenderer, img);
  SDL_FreeSurface(img);
  if(imagem == NULL){
    printf("Erro ao carregar imagem do Lixo %d!\n", tipo);
  }
}

void Lixo::desenha(){
  SDL_Rect rect = {(int)x-(largura/2), (int)y-(altura/2), largura, altura};
  SDL_RenderCopy(gRenderer, imagem, NULL, &rect);
}
