#ifndef FASE2_H
#define FASE2_H

#include <SDL2/SDL.h>

#include "Mergulhador.hpp"
#include "Caranha.hpp"
#include "Miraguaia.hpp"

class Fase2{
private:
	SDL_Renderer* gRenderer;
	int alturaTela;
	int larguraTela;
	// Mergulhador mergulhador;
	// Caranha caranha[];
	// Miraguaia miraguaia[];

public:
	Fase2();
};
#endif
