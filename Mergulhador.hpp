#ifndef MERGULHADOR_H
#define MERGULHADOR_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

#include "Objeto.hpp"

#define CAINDO 0
#define DIREITA 1
#define ESQUERDA 2
#define CIMA 3
#define BAIXO 4
#define CIMAESQUERDA 5
#define BAIXOESQUERDA 6
#define CIMADIREITA 7
#define BAIXODIREITA 8


class Mergulhador : public Objeto{

public:
	int oxigenio;
	int fase;
	int imagem;
	int estado;
	SDL_Texture* vetImagens[4];


public:
	Mergulhador();
  Mergulhador(SDL_Renderer*, int, int, int);
	void run(unsigned int);
	void desenha();
	void bonusOxigenio(int quant);
	void mover(int);
	void moverDireita(unsigned int);
	void moverEsquerda(unsigned int);
	void moverBaixo(unsigned int);
	void moverCima(unsigned int);
	void carregarImagem();

};
#endif
