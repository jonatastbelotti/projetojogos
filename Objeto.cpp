#include "Objeto.hpp"

Objeto::Objeto(SDL_Renderer* t, int alt, int lar){
  gRenderer = t;
  larguraTela = alt;
  alturaTela = lar;
  desenhadoUltimaVez = SDL_GetTicks();
}

bool Objeto::colidiu(Objeto *obj1, Objeto *obj2){
  SDL_Rect o1 = {(obj1->x)-((obj1->largura)/2), (obj1->y)-((obj1->altura)/2), obj1->largura, obj1->altura};
  SDL_Rect o2 = {(obj2->x)-((obj2->largura)/2), (obj2->y)-((obj2->altura)/2), obj2->largura, obj2->altura};
  SDL_Rect r = {(obj2->x)-((obj2->largura)/2), (obj2->y)-((obj2->altura)/2), obj2->largura, obj2->altura};

  if(SDL_TRUE == SDL_IntersectRect(&o1, &o2, &r) && ((r.w * r.h)  > 300)){
    // printf("x = %d\ny = %d\nlar = %d\nalt = %d\n", r.x, r.y, r.w, r.h);
    return true;
  }

  // if((obj1->x + (obj1->largura)/2) > (obj2->x - (obj2->largura)/2) && (obj1->y + (obj1->altura)/2 > obj2->y - (obj2->altura)/2)){
  //   return true;
  // }
  // if((obj1->x-(obj1->largura)/2 < (obj2->x + obj2->largura/2)) && ((obj1->y+obj1->altura/2) > (obj2->y-obj2->altura/2))){
  //   return true;
  // }
  // if(((obj1->x+(obj1->largura)/2)>(obj2->x-obj2->largura/2)) && ((obj1->y-obj1->altura/2)<(obj2->y+obj2->altura/2))){
  //   return true;
  // }
  // if(((obj1->x-(obj1->largura)/2) < (obj2->x+obj2->largura/2)) && ((obj1->y-obj1SDlsdsd->altura/2) < (obj2->y+obj2->altura/2))){
  //   return true;
  // }

  return false;
}
