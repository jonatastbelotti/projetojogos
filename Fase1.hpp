#ifndef FASE1_H
#define FASE1_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <vector>
#include <list>
#include <iostream>
#include <assert.h>
#include <time.h>

#include "Pampo.hpp"
#include "TubaraoLixa.hpp"
#include "BaiacuPintado.hpp"
#include "Mergulhador.hpp"
#include "Lixo.hpp"
#include "Peixe.hpp"

#define FINALTELA 500

class Fase1{
private:
	SDL_Renderer* gRenderer;
	SDL_Texture* fundoTela;
	SDL_Texture* imagemOxigenio;
	SDL_Rect rect;
	int alturaTela;
	int larguraTela;
	float jaAndou;
	float velFundoY;
	float y1;
	float y2;
	unsigned int ultMovFundo;
	unsigned int ultLixCria;
	unsigned int ultPexCria;
	unsigned int venceu;
	Mergulhador *mergulhador;
	std::vector<Peixe*> listaPeixes;
	std::vector<Lixo*> listaLixos;

	public:
		Fase1(int, int, SDL_Renderer*);
		void carregarImagem();
		void desenhaPlacar();
		void desenhaVenceu();
		void desenhaFundo();
		void desenhaLixos();
		void desenhaPeixes();
		void desenha();
		void destroi();
		void movimenta();
		void movimentaFundo();
		void movimentaObjetos(float dy);
		void runPeixes(unsigned int);
		void criaObjetos();
		void criaLixos();
		void criaPeixes();
		void colisao();
		void morreu();
		int run();
};
#endif
