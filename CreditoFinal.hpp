#ifndef CREDITO_FINAL_H
#define CREDITO_FINAL_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <time.h>



class CreditoFinal{
private:
  SDL_Renderer* gRenderer;
  SDL_Rect creditos;
  float y;
  float velocidadeY;
  int alturaTela;
  int larguraTela;
  SDL_Texture *imagem;
  unsigned int ultimoMovimento;

public:
  CreditoFinal();
  CreditoFinal(int, int, SDL_Renderer*);
  void carregarImagem();
  int run();
  void moveTextos();
};
#endif
