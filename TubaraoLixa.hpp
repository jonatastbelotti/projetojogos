#ifndef TUBARAO_LIXA_H
#define TUBARAO_LIXA_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Peixe.hpp"

class TubaraoLixa : public Peixe{
public:
  TubaraoLixa(SDL_Renderer* tela, int l, int a, int f);
  void carregarImagem();
  void run(unsigned int, int, int);
};
#endif
