#ifndef BAIACU_PINTADO_H
#define BAIACU_PINTADO_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "Peixe.hpp"

class BaiacuPintado : public Peixe{
public:
  BaiacuPintado(SDL_Renderer* tela, int l, int a, int f);
  void carregarImagem();
  void mover(unsigned int, int, int);
  void run(unsigned int, int, int);
};
#endif
