all: objetos run

objetos:
	g++ -c -w Objeto.cpp `sdl2-config --cflags --libs` -lSDL2_image
	g++ -c -w CreditoInicial.cpp `sdl2-config --cflags --libs` -lSDL2_image
	g++ -c -w CreditoFinal.cpp `sdl2-config --cflags --libs` -lSDL2_image
	g++ -c -w HistoriaFinal.cpp `sdl2-config --cflags --libs`
	g++ -c -w HistoriaInicial.cpp `sdl2-config --cflags --libs`
	g++ -c -w Cardapio.cpp `sdl2-config --cflags --libs`
	g++ -c -w Controles.cpp `sdl2-config --cflags --libs`
	g++ -c -w Menu.cpp `sdl2-config --cflags --libs`
	g++ -c -w Mergulhador.cpp
	g++ -c -w Peixe.cpp `sdl2-config --cflags --libs` -lSDL2_image
	g++ -c -w Lixo.cpp `sdl2-config --cflags --libs` -lSDL2_image
	g++ -c -w BaiacuPintado.cpp `sdl2-config --cflags --libs` -lSDL2_image
	g++ -c -w Caranha.cpp
	g++ -c -w Miraguaia.cpp
	g++ -c -w Pampo.cpp `sdl2-config --cflags --libs` -lSDL2_image
	g++ -c -w RaiaPrego.cpp
	g++ -c -w TubaraoLixa.cpp `sdl2-config --cflags --libs` -lSDL2_image
	g++ -c -w Fase1.cpp `sdl2-config --cflags --libs` -lSDL2_image
	g++ -c -w Fase2.cpp `sdl2-config --cflags --libs`
	g++ -c -w Fase3.cpp `sdl2-config --cflags --libs`

run:
	g++ -w -o jogo  Main.cpp `sdl2-config --cflags --libs` -lSDL2_image -lSDL2_ttf CreditoInicial.o CreditoFinal.o HistoriaFinal.o HistoriaInicial.o Cardapio.o Controles.o Fase1.o Objeto.o Mergulhador.o Lixo.o Peixe.o Pampo.o BaiacuPintado.o TubaraoLixa.o
	./jogo
