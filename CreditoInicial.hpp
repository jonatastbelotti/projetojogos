#ifndef CREDITO_INICIAL_H
#define CREDITO_INICIAL_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

class CreditoInicial{
private:
  SDL_Renderer* gRenderer;
  int alturaTela;
  int larguraTela;
  SDL_Texture *fundo;

public:
  CreditoInicial();
  CreditoInicial(int, int, SDL_Renderer*);
  void carregarImagem();
  int run();
};
#endif
